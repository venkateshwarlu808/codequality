<?php

namespace SomeNamespace;

class SomeClass
{
    /** @var string just one field */
    public string $field;

    /** @var string just another field */
    public string $field2;
}
